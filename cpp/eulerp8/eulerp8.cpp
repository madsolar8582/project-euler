//Project Euler Problem #8
//Find the greatest product of five consecutive digits in the 1000-digit number.

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

int main()
{
  vector<int> longNum;
  string longNumFile = "ep8num.txt";
  ifstream fileIn;
  char charIn;
  int result;
  int biggestResult = 0;

  fileIn.open(longNumFile.c_str());
  if(fileIn.fail())
  {
    cerr << "Unable to open " << longNumFile << " !" << endl;
    fileIn.close();
    exit(1);
  }
  while(!fileIn.eof())
  {
    fileIn >> charIn;
    longNum.push_back(charIn - '0');
  }
  fileIn.close();  
  for(int i = 0; i < longNum.size(); i++)
  {
    result = 1;
    for(int j = 0; j < 5; j++)
    {
      result *= longNum[i + j];
    }
    if(result > biggestResult)
    {
      swap(result, biggestResult);
    }
  }

  cout << "The greatest product is: " << biggestResult << endl;

  return 0;
}

