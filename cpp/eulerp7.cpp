//Project Euler Problem #7
//What is the 10001st prime number?

#include <iostream>
#include <cmath>

using namespace std;

bool isPrimeNumber(int testNum);

int main()
{
  int count = 0;
  int calcNum = 2; 
  while(true)
  {
    if(isPrimeNumber(calcNum))
    {
      count++;
      if(count == 10001)
      {
        break; 
      }
    }
    calcNum++;
  }
 
  cout << "The 10001st prime is: " << calcNum << endl;

  return 0;
}

bool isPrimeNumber(int testNum) 
{
  if((testNum % 2 == 0) && (testNum != 2))
  {
    return false;
  }
  for(int i = 3; i <= (floor(pow(testNum, .5)) + 1); i += 2)
  {
    if(testNum % i == 0)
    {
      return false;
    }
  }

  return true;
}

