//Project Euler Problem #2
//By considering the terms in the Fibonacci sequence whose values do not exceed
//4000000, find the sum of the even-valued terms.

#include <iostream>

using namespace std;

unsigned int calcFib();

int main()
{
  unsigned int sum;
  sum = calcFib();
  cout << "The sum is: " << sum << endl;
  return 0;
}

unsigned int calcFib()
{
  unsigned int fib1, fib2, temp, total;
  fib1 = 0;
  fib2 = 1;
  temp = 0;
  total = 0;
  while(fib2 <= 4000000)
  {
    temp = fib1 + fib2;
    fib1 = fib2;
    fib2 = temp;
    if(fib2 % 2 == 0)
    {
      total += fib2;
    }
  }
  return total;
}

