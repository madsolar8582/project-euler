//Project Euler Problem #10
//Find the sum of all the primes below two million.

#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(unsigned long num);

int main()
{
  unsigned long sum = 0;

  for(unsigned long sentinel = 2; sentinel < 2000000; sentinel++)
  {
    if(isPrime(sentinel))
    {
      sum += sentinel;
    }
  }

  cout << "The sum is: " << sum << endl;

  return 0;
}

bool isPrime(unsigned long num)
{
  if((num % 2 == 0) && (num != 2))
  {
    return false;
  }
  for(unsigned long i = 3; i <= (floor(pow(num, .5)) + 1); i += 2)
  {
    if(num % i == 0)
    {
      return false;
    }
  }

  return true;
}

