//Project Euler Problem #11
//What is the greatest product of four adjacent numbers in any directon in 
//the 20×20 grid?

#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdlib>

using namespace std;

int main()
{
  int grid[20][20];
  string gridFile = "ep11grid.txt";
  ifstream fileIn;
  char charIn;
  long product;
  long biggestProduct = 0;
  
  fileIn.open(gridFile.c_str());
  if(fileIn.fail())
  {
    cerr << "Unable to open " << gridFile << " !" << endl;
    fileIn.close();
    exit(1);
  }
  for(int i = 0; i < 20; i++)
  {
    for(int j = 0; j < 20; j++)
    {
      fileIn >> grid[i][j];
    }
  }
  fileIn.close();

  for(int r = 0; r < 20; r++)
  {
    for(int c = 0; c < 20; c++)
    {
      if(c < 17)
      {
        product = grid[r][c] * grid[r][c+1] * grid[r][c+2] * grid[r][c+3];
        if(product > biggestProduct)
        {
           swap(product, biggestProduct);
        }
      }
      if(r < 17)
      {
        product = grid[r][c] * grid[r+1][c] * grid[r+2][c] * grid [r+3][c];
        if(product > biggestProduct)
        {
          swap(product, biggestProduct);
        }

        if(c < 17)
        {
          product = grid[r][c] * grid[r+1][c+1] * grid[r+2][c+2] * grid[r+3][c+3];
          if(product > biggestProduct)
          {
            swap(product, biggestProduct);
          }
        }

        if(c > 3)
        {
          product = grid[r][c] * grid[r+1][c-1] * grid[r+2][c-2] * grid[r+3][c-3];
          if(product > biggestProduct)
          {
            swap(product, biggestProduct);
          }
        }
      }
    }
  }

  cout << "The greatest product is: " << biggestProduct << endl;
  return 0;
}

