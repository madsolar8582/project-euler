//Project Euler Problem #13
//Work out the first ten digits of the sum of the one-hundred 50-digit numbers.
//Compile: clang++ -lgmp eulerp13.cpp -o eulerp13

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <gmp.h>

using namespace std;

int main()
{
  mpz_t numbers[100];
  mpz_t sum;
  mpz_init(sum);
  for(int i = 0; i < 100; i++)
  {
    mpz_init(numbers[i]);
  }
  
  string numberFile = "ep13nums.txt";
  ifstream fileIn;
  string number = "";
  
  fileIn.open(numberFile.c_str());
  if(fileIn.fail())
  {
    cerr << "Unable to open " << numberFile << " !" << endl;
    fileIn.close();
    exit(1);
  }
  int numCount = 0;
  while((!fileIn.eof()) && (numCount < 100))
  {
    fileIn >> number;
    mpz_set_str(numbers[numCount], number.c_str(), 10);
    numCount++;
  }
  fileIn.close();
  
  mpz_set(sum, numbers[0]);
  for(int i = 1; i < 100; i++)
  {
    mpz_add(sum, sum, numbers[i]);
  }
  
  char* temp;
  mpz_get_str(temp, 10, sum);
  string temp2(temp);
  string result = temp2.substr(0,10);
  cout << "The first 10 digits of 100 50-digit numbers is: " << result << ".";
  cout << endl;
  
  return 0;
}
