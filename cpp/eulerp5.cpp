//Project Euler Problem #5
//What is the smallest positive number that is evenly divisible by all of the n-
//umbers from 1 to 20?

#include <iostream>

using namespace std;

int main()
{
  unsigned int num = 1;

  while(true)
  {
    if((num % 11 == 0) && (num % 12 == 0) && (num % 13 == 0) && (num % 14 == 0)
       && (num % 15 == 0) && (num % 16 == 0) && (num % 17 == 0) && 
       (num % 18 == 0) && (num % 19 == 0) && (num % 20 == 0))
    {
      break;
    }
    num++;
  } 
  
  cout << "The smallest positive number is: " << num << endl;
  return 0;
}

