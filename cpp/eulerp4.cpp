//Project Euler Problem #4
//Find the largest palindrome made from the product of two 3-digit numbers.

#include <iostream>

using namespace std;

bool isPalindrome(unsigned long x);

int main()
{
  unsigned long calcNum = 0, palindrome = 0;

  for(int i = 1; i < 1000; i++)
  {
    for(int j = 1; j < 1000; j++)
    {
      calcNum = i * j;
    
      if(isPalindrome(calcNum))
      {
        if(calcNum > palindrome)
        {
          palindrome = calcNum;
        }
      }
    }
  }

  cout << "The max palindrome is: " << palindrome << endl;
  return 0;
}

bool isPalindrome(unsigned long x)
{
  unsigned long y, z, total = 0;
  z = x;

  while(x)
  {
    y = x % 10;
    x /= 10;
    total *= 10;
    total += y;
  }

  if(z == total)
  {
    return true;
  }
  else
  {
    return false;
  }
}

