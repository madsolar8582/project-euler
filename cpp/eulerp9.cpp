//Project Euler Problem #9
//There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//Find the product abc.

#include <iostream>

using namespace std;

int main()
{
  long product;

  for(int a = 1; a <= 500; a++)
  {
    for(int b = a; b <= 500; b++)
    {
      for(int c = b; c <= 500; c++)
      {
        if((((a*a) + (b*b)) == (c*c)) && (a+b+c == 1000))
        {
          product = a * b * c;
          break;
        }
      }
    }
  }  

  cout << "The Pythagorean triplet's product is: " << product << endl;

  return 0;
}

