//Project Euler Problem #15
//How many routes are there through a 20×20 grid?

#include <iostream>

using namespace std;

unsigned long long binomial(unsigned long long n, unsigned long long k);

int main()
{
  //Binomial Theorem Problem (n choose k -> Combinatorics: n!/(k! * n-k)!)
  //Use Binomial Coefficients from Pascal's Triangle

  int numRows = 20;
  int numColumns = 20;
  unsigned long long numPaths;
  
  numPaths = binomial((numRows + numColumns), numColumns);

  cout << "The number of paths is: " << numPaths << "." << endl;

  return 0;
}

unsigned long long binomial(unsigned long long n, unsigned long long k)
{
  if(k > (n - k))
  {
    k = n - k;
  }

  unsigned long long coefficient = 1;

  for(unsigned long long i = 0; i < k; i++)
  {
    coefficient *= (n - i);
    coefficient /= (i + 1);
  }
 
  return coefficient;
}

