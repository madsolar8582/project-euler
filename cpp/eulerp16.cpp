//Project Euler Problem #16
//What is the sum of the digits of the number 2^1000?
//Compile: clang++ -lgmp eulerp16.cpp -o eulerp16

#include <iostream>
#include <vector>
#include <gmp.h>

using namespace std;

int main()
{
  mpz_t num;
  mpz_t base;
  mpz_init(num);
  mpz_init(base);
  mpz_set_ui(base, static_cast<unsigned long int>(2));
  
  mpz_pow_ui(num, base, 1000);
  char *temp;
  temp = new char;
  mpz_get_str(temp, 10, num);
  string rawDigits(temp);
  vector<int> digits;
    
  for(string::iterator i = rawDigits.begin(); i != rawDigits.end(); i++)
  {
    digits.push_back(*i - '0');
  }

  int sum = 0;
  for(int i = 0; i < digits.size(); i++)
  {
    sum += digits[i];
  }
    
  cout << "The sum of the digits of 2^1000 is: " << sum << "." << endl;
  
  delete temp;
  return 0;
}
