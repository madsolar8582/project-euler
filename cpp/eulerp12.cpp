//Project Euler Problem #12
//What is the value of the first triangle number to have over five hundred 
//divisors?

#include <iostream>
#include <cmath>

using namespace std;

int divisors(unsigned long num);

int main()
{
  unsigned long triangleNum = 0;
  unsigned long naturalNums = 1;

  while(divisors(triangleNum) < 500)
  {
    triangleNum += naturalNums;
    naturalNums++;
  }

  cout << "The first triangle number is: " << triangleNum << endl;
  return 0;
}

int divisors(unsigned long num)
{
  int numOfDivisors = 0;
  unsigned long squareRoot = static_cast<unsigned long>(sqrt(num));

  for(unsigned long i = 1; i <= squareRoot; i++)
  {
    if(num % i == 0)
    {
      numOfDivisors += 2;
    }
  }
  if(pow(static_cast<double>(squareRoot), 2) == num)
  {
    numOfDivisors--;
  }

  return numOfDivisors;
}

