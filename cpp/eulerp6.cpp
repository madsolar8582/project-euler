//Project Euler Problem #6
//Find the difference between the sum of the squares of the first one hundred 
//natural numbers and the square of the sum.

#include <iostream>

using namespace std;

int main()
{
  unsigned int sumNatural = 0, sumSquare = 0;

  for(unsigned int i = 1; i < 101; i++)
  {
    sumNatural += i;
    sumSquare += (i * i);
  }

  cout << "The difference is: " << (sumNatural * sumNatural) - sumSquare << endl;
  return 0;
}

