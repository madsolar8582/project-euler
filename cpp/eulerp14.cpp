//Project Euler Problem #14
//Which starting number, under one million, produces the longest Collatz chain?

#include <iostream>

using namespace std;

int main()
{
  unsigned long seqNum;
  int chainTracker;
  int startNum;
  int maxChain = 0;

  for(int i = 1; i < 1000000; i++)
  {
    chainTracker = 1;
    seqNum = i;

    while(seqNum != 1)
    {
      if((seqNum % 2) != 0)
      {
        seqNum = ((3 * seqNum) + 1);
      }
      else
      {
        seqNum /= 2;
      }
      chainTracker++;
    }
    
    if(chainTracker > maxChain)
    {
      maxChain = chainTracker;
      startNum = i;
    }
  }

  cout << "The longest Collatz chain began at: " << startNum << "." << endl;

  return 0;
}
