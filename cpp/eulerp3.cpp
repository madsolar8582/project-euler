//Project Euler Problem #3
//What is the largest prime factor of the number 600851475143?

#include <iostream>
#include <vector>

using namespace std;

int main()
{
  unsigned long bigNum = 600851475143;
  vector<unsigned long> divisors;
  unsigned long d;
  
  while (bigNum > 1)
  {
    d = 3; //Since bigNum is odd, all prime divisors must be odd.
    while (bigNum % d != 0)
    {
      d += 2; 
    }
    divisors.push_back(d);
    bigNum /= d;
  }

  cout << "The largest prime factor is: " << divisors.back() << endl;
  return 0;
}

