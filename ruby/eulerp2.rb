#!/usr/bin/ruby
# Project Euler Problem 2
# By considering the terms in the Fibonacci sequence whose values do not exceed four 
# million, find the sum of the even-valued terms.

fib1 = fib2 = 1                        # <- Multiple Assignments is sweet
total = 0
while fib2 <= 4000000
  total += fib2 unless fib2.odd?
  fib1,fib2 = fib2,fib1+fib2           # <- Variable Swapping is awesome
end
puts "#{total}\n"

