#!/usr/bin/ruby
# Project Euler Problem 1
# Find the sum of all the multiples of 3 or 5 below 1000.

x = (1..999).select { |i| (i % 3 == 0) || (i % 5 == 0) }.inject { |total, i| total + i }
puts "#{x}\n"

